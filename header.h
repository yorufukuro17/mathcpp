#ifndef HEADER_H
#define HEADER_H

//Сокращенная версия SetConsoleTextAttribute()
#define COLOR_CONTROL(object, color_number) SetConsoleTextAttribute(object, color_number)

#define VERSION 0.1

//Стандартные математические операции
int add(int a, int b);
int substract(int a, int b);
int multiple(int a, int b);
int division(int a, int b);

void argument_control(int c, char *v[]);
void into_console_mode();


//Для консольного режима

class MathOperation
{	
private:
	int operation_count;

public:
    MathOperation();

public:
	double _add(double a, double b);
	double _substract(double a, double b);
	double _multiple(double a, double b);
	double _multiple(double a, double start, double end, HANDLE h);
    void all_operation(double a, double start, double end);

	double _division(double a, double b);

public:
	int get_operation_count();
	void set_operation_count(int n); //ЗАЧЕМ?

	void test1();
};


class ByteOperation
{
private:
    int operation_count;

public:
	ByteOperation();

public:
    int _shift_left(int a, int b);
    int _shift_right(int a, int b);

	int _conjuction(int a, int b);
	int _disjunction(int a, int b);
	int _bitwise_exclusive(int a, int b);

public:
	int get_operation_count();
	//А где сеттер сукин сын!!!
};

#endif 