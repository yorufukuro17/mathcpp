#include <iostream>
#include <string.h>
#include <windows.h>
#include "header.h"

using namespace std;
/*
Сложение                |   add
Вычитание               |   substract
Умножение               |   multiple
Деление                 |   division
Возведение в степень    |   degree
Извлечение корня        |   root_ext
Логарифмирования        |   logariphm
Тетрация                
*/


//Контроль каждым агрументом
void argument_control(int c, char *v[])
{
    //Для легкой записи проверки массива v[]
    #define _STRCMP(txt) _stricmp(v[1], txt)

    HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);

    //Переменные для операции
    int a, b;
    

	if (c <= 1) { 
        //Если запустить программу не на консоли то он запустится в виде консольного режима
        into_console_mode();
	}
	if (!_STRCMP("add")) {

        // Операция сложения     
		if (sscanf(v[2], "%d", &a) && sscanf(v[3], "%d", &b)) {


            COLOR_CONTROL(h, 0x09);
            cout << "---> ";
            COLOR_CONTROL(h, 0x07);
            
			cout << a << " + " << b << " = " 
			    << add(a, b) << endl;
		}

	}
    else if (!_STRCMP("substract")) {

        //Операция вычитания
    	if (sscanf(v[2], "%d", &a) && sscanf(v[3], "%d", &b)) {
            

            COLOR_CONTROL(h, 0x09);
            cout << "---> ";
            COLOR_CONTROL(h, 0x07);

    		cout << a << " - " << b << " = " 
    		     << substract(a, b) << endl;
    	}

    }
    else if (!_STRCMP("multiple")) {
        
        //Операция умножения
    	if (sscanf(v[2], "%d", &a) && sscanf(v[3], "%d", &b)) 
    	{
  

    		COLOR_CONTROL(h, 0x09);
            cout << "---> ";
            COLOR_CONTROL(h, 0x07);

    		cout << a << " * " << b << " = " 
    		     << multiple(a, b) << endl;

    	}
    	else if (sscanf(v[2], "%d", &a)) 
    	{
    	   int start, end;
           
           // Умножение нескольких чисел (от start до end)
    	   if (!_stricmp(v[3], "--start") && sscanf(v[4], "%d", &start) &&
    	   	   !_stricmp(v[5], "--end")   && sscanf(v[6], "%d", &end)) 
    	   {
               COLOR_CONTROL(h, 0x09);
               cout << "######## ";
               COLOR_CONTROL(h, 0x07);
               
               cout << a << " * " << "(" << start << "..." << end << ")";
               
               COLOR_CONTROL(h, 0x09);
               cout << " ########";
               COLOR_CONTROL(h, 0x07);

               cout << endl;
               

    	   	   while (start <= end) {
    	   	   	    COLOR_CONTROL(h, 0x09);
                    cout << "-> ";
                    COLOR_CONTROL(h, 0x07);


    	   	   	    cout << a << " * " << start << " = " << multiple(a, start) << endl;
    	   	   	    start++;
    	   	   }
    	   } 


    	}
    }
    else if (!_STRCMP("division")) {
        
        //Операция деления
    	if (sscanf(v[2], "%d", &a) && sscanf(v[3], "%d", &b)) {


    		COLOR_CONTROL(h, 0x09);
            cout << "---> ";
            COLOR_CONTROL(h, 0x07);

    		cout << a << " / " << b << " = " 
    		     << division(a, b) << endl;
    	}

    }
    else {
        //Вывод несуществующей комманды
        COLOR_CONTROL(h, 0x09);
        cout << "---> ";
        COLOR_CONTROL(h, 0x07);

    	cout << "Command \"" << v[1] << "\" not found" << endl;
    }

}
double test_1(double a, double start, double end) { 
    
    double res;
    
    if (start > end) {
        return end;
    }
    
    start++;
    res = a * start;
    cout << start << " * "<< a << " = " << (int)res << endl;

    return test_1(a, start , end);
}

int main(int argc, char *argv[])
{
    
    argument_control(argc, argv);

	return 0;
}




//Краткая запись a
static void 
point_a(HANDLE h) 
{
    COLOR_CONTROL(h, 0x09);
    cout << "-> a:";
    COLOR_CONTROL(h, 0x07);
}




//Краткая запись b
static void 
point_b(HANDLE h) 
{
    COLOR_CONTROL(h, 0x09);
    cout << "-> b:";
    COLOR_CONTROL(h, 0x07);
}



static void 
help_for_console_mode()
{
    string txt[] = {
        "Command list here\n",

        "--Math operation",
        "add:\t\t\ta + b = res",
        "substract:\t\ta - b = res",
        "multiple:\t\ta * b = res",
        "multiple-some-num:\ta * (start...end) = res1, res2, res3...",
        "division:\t\ta \\ b = res\n",

        "--Other",
        "clear\t\tClear console like from real console)))",
        "exit\t\tExit from your life, lozer >:)"

    };


    for (string t : txt) cout << t << endl; 

}

void into_console_mode() 
{
    #define MACRO_CASE(txt) console_control == txt
    
    HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
    int oc;
    string console_control;
    MathOperation op;
    ByteOperation bp;


    
    COLOR_CONTROL(h, 0x0b);
    cout << "WELCOME TO THE MATH++!" << endl << "version:\t" << VERSION << endl << endl;
    

    while (true) //Цикл работы ввода на консоли
    {
        double a, b;
        int start, end;
        int bt_a, bt_b;

        oc = op.get_operation_count() + bp.get_operation_count();

        COLOR_CONTROL(h, 0x09);
        cout << "[OQ:" << oc <<"]"<< "---> ";
        COLOR_CONTROL(h, 0x07);

        cin >> console_control;

        if (MACRO_CASE("help")) 
        {
            help_for_console_mode();
        }
        if (MACRO_CASE("add")) 
        {
            point_a(h);
            cin >> a;
            
            point_b(h);
            cin >> b; 
            
            COLOR_CONTROL(h, 0x09);
            cout << "[res]"<< "---> ";
            COLOR_CONTROL(h, 0x07);
            
            cout << a << " + " << b << " = " << op._add(a, b) << endl;

            COLOR_CONTROL(h, 0x07);
        }
        if (MACRO_CASE("substract"))
        {
            point_a(h);
            cin >> a;

            point_b(h);
            cin >> b; 
            
            COLOR_CONTROL(h, 0x09);
            cout << "[res]"<< "---> " << 
                 a << " - " << b << " = " << op._substract(a, b) << endl;
            COLOR_CONTROL(h, 0x07);
        }
        if (MACRO_CASE("multiple"))
        {
            point_a(h);
            cin >> a;

            point_b(h);
            cin >> b; 
            
            COLOR_CONTROL(h, 0x09);
            cout << "[res]"<< "---> " << 
                 a << " * " << b << " = " << op._multiple(a, b) << endl;
        }
        if (MACRO_CASE("multiple-some-num")) 
        {
            point_a(h);
            cin >> a;

            COLOR_CONTROL(h, 0x09);
            cout << "-> start: ";
            COLOR_CONTROL(h, 0x09);
            cin >> start;

            COLOR_CONTROL(h, 0x09);
            cout << "-> end: ";
            COLOR_CONTROL(h, 0x07);
            cin >> end;

            cout << op._multiple(a, start, end, h) << endl;
        
        }
        if (MACRO_CASE("division")) 
        {
            point_a(h);
            cin >> a;

            point_b(h);
            cin >> b; 
            
            COLOR_CONTROL(h, 0x09);
            cout << "[res]"<< "---> " << 
                 a << " / " << b << " = " << op._division(a, b) << endl;
        }


        if (MACRO_CASE("shift-left"))
        {
            point_a(h);
            cin >> bt_a;

            point_b(h);
            cin >> bt_b;

            COLOR_CONTROL(h, 0x09);
            cout << "[res]"<< "---> " << 
                 bt_a << " << " << bt_b << " = " << bp._shift_left(bt_a, bt_b) << endl;
        }
        if (MACRO_CASE("shift-right"))
        {
            point_a(h);
            cin >> bt_a;

            point_b(h);
            cin >> bt_b;

            COLOR_CONTROL(h, 0x09);
            cout << "[res]"<< "---> " << 
                 bt_a << " << " << bt_b << " = " << bp._shift_right(bt_a, bt_b) << endl;
        }
        if (MACRO_CASE("conjuction"))
        {
            point_a(h);
            cin >> bt_a;

            point_b(h);
            cin >> bt_b;

            COLOR_CONTROL(h, 0x09);
            cout << "[res]"<< "---> " << 
                 bt_a << " & " << bt_b << " = " << bp._conjuction(bt_a, bt_b) << endl;
        }
        if (MACRO_CASE("disjunction"))
        {
            point_a(h);
            cin >> bt_a;

            point_b(h);
            cin >> bt_b;

            COLOR_CONTROL(h, 0x09);
            cout << "[res]"<< "---> " << 
                 bt_a << " | " << bt_b << " = " << bp._conjuction(bt_a, bt_b) << endl;
        }
        if (MACRO_CASE("bitwise-exclusive"))
        {
            point_a(h);
            cin >> bt_a;

            point_b(h);
            cin >> bt_b;

            COLOR_CONTROL(h, 0x09);
            cout << "[res]"<< "---> " << 
                 bt_a << " ^ " << bt_b << " = " << bp._conjuction(bt_a, bt_b) << endl;
        }
        if (MACRO_CASE("clear")) { system("cls"); }
        if (MACRO_CASE("exit")) { exit(1); }

    }

}



int add(int a, int b)       { return a + b; }
int substract(int a, int b) { return a - b; }
int multiple(int a, int b)  { return a * b; }
int division(int a, int b)  { return a / b; }

/*
=========================================================
  ---- Поле для определения класса MathOperation ----
========================================================
*/

MathOperation::MathOperation() {
    operation_count = 0;
}

double MathOperation::_add(double a, double b) 
{
    operation_count += 1;
    return a + b;
}

double MathOperation::_substract(double a, double b)
{
    operation_count += 1;
    return a - b;
}

double MathOperation::_multiple(double a, double b) 
{
    operation_count += 1;
    return a * b;
}

double MathOperation::_multiple(double a, double start, double end, HANDLE h)
{
    double res;
    
    if (start > end) {
        return NULL;
    }
    
    operation_count++;
    start++;

    res = a * start;
    cout << a << " * "<< start << " = " << (int)res << endl;

    return test_1(a, start , end);

}

double MathOperation::_division(double a, double b) 
{
    operation_count += 1;
    return a / b;
}

void MathOperation::test1() 
{
    operation_count += 1;
    cout << "Hello ?" << endl;
}

int MathOperation::get_operation_count() 
{    
    return operation_count;
}



/*
=========================================================
  ---- Поле для определения класса ByteOperation ----
=========================================================
*/


ByteOperation::ByteOperation() 
{
    operation_count = 0;
}

int ByteOperation::_shift_left(int a, int b)
{
    operation_count += 1;
    return a << b;
}

int ByteOperation::_shift_right(int a, int b)
{
    operation_count += 1;
    return a >> b;
}

int ByteOperation::_conjuction(int a, int b)
{
    operation_count += 1;
    return a & b;
}

int ByteOperation::_disjunction(int a, int b) 
{
    operation_count += 1;
    return a | b;
}

int ByteOperation::_bitwise_exclusive(int a, int b)
{
    operation_count += 1;
    return a ^ b;
}

int ByteOperation::get_operation_count()
{
    return operation_count;
}
